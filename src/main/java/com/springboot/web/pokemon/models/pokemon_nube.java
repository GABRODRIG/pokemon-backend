package com.springboot.web.pokemon.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "pokemon_nube") // Definición de nombre de la tabla en BD.
public class pokemon_nube {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String name;
	private String url;
	@Column(name = "registry_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date crearFecha;
	
	/** Antes de hacer un insert de este objeto en bd, 
	 *  asigne la fecha utilizando la anotación PrePersist(propia de JPA)
	 */
	@PrePersist
	public void insertarFecha() {
		this.crearFecha = new Date();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Date getCrearFecha() {
		return crearFecha;
	}

	public void setCrearFecha(Date crearFecha) {
		this.crearFecha = crearFecha;
	}

}
