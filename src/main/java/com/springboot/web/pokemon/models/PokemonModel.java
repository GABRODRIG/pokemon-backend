package com.springboot.web.pokemon.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pokemones") // Definición de nombre de la tabla en BD.
public class PokemonModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String nombre;
	private String genero;
	private String habilidad;
	private String transformacion;
	
	public PokemonModel(Integer id, String nombre, String genero, String habilidad, String transformacion) {
		this.id = id;
		this.nombre = nombre;
		this.genero = genero;
		this.habilidad = habilidad;
		this.transformacion = transformacion;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getHabilidad() {
		return habilidad;
	}

	public void setHabilidad(String habilidad) {
		this.habilidad = habilidad;
	}

	public String getTransformacion() {
		return transformacion;
	}

	public void setTransformacion(String transformacion) {
		this.transformacion = transformacion;
	}

}
