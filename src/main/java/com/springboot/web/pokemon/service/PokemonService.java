package com.springboot.web.pokemon.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.web.pokemon.models.PokemonModel;
import com.springboot.web.pokemon.repository.PokemonRepository;

@Service
public class PokemonService {
	
	@Autowired
	PokemonRepository pokemonRepository; //Para utilizar el repositorio lo declaramos como una variable
	
		public PokemonModel guardarPokemon(PokemonModel pokemon) {
			return pokemonRepository.save(pokemon); //Le pasamos como parámetro el pokemon que guardaremos
		}
		
		public Optional<PokemonModel> obtenerPorId(Integer id){
			return pokemonRepository.findById(id);
		}
		
		//Creación de método de tipo ArrayList, regresando varios objetos de tipo PokemonModel
		public ArrayList<PokemonModel> obtenerPokemones(){
			/*findAll: Método que obtiene todos los registros
			 * Tranformar a un arreglo de tipo PokemonModel, para poder regresarlo en un JSON */
			return (ArrayList<PokemonModel>)pokemonRepository.findAll();
		}
		
		//Eliminar pokemon por id
				public boolean eliminarPokemon(Integer id){
					try {
						pokemonRepository.deleteById(id);
						System.out.println("Pokemon eliminado " + id);
						return true;
					} catch (Exception err) {
						System.out.println("¡Error al intentar eliminar el pokemon! " + id);
						return false;
					}
				}
		
}
