package com.springboot.web.pokemon.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.web.pokemon.models.pokemon_nube;
import com.springboot.web.pokemon.repository.poblarTablaRepository;

@Service
public class poblarTablaService {
	
	@Autowired
	poblarTablaRepository poblartablaRepository; //Para utilizar el repositorio lo declaramos como una variable
	
	
	public pokemon_nube guardarInfo(pokemon_nube pokemonNube) {
		return poblartablaRepository.save(pokemonNube); //Le pasamos como parámetro el pokemon que guardaremos
	}

}
