package com.springboot.web.pokemon.repository;

import org.springframework.data.repository.CrudRepository;

import com.springboot.web.pokemon.models.PokemonModel;

public interface PokemonRepository extends CrudRepository<PokemonModel, Integer>{

}
