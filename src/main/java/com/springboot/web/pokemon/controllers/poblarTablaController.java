package com.springboot.web.pokemon.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.web.pokemon.models.pokemon_nube;
import com.springboot.web.pokemon.service.poblarTablaService;

@RestController
@RequestMapping("/inicio")
public class poblarTablaController {

	@Autowired
	poblarTablaService poblartablaService;

	@PostMapping("/poblar-tabla")
	public pokemon_nube poblarTabla(@RequestBody pokemon_nube pokemonNube) { //Recibe como parámetro un @RequestBody, se usa para tomar la información y la guarda en el objeto pokemon
		return poblartablaService.guardarInfo(pokemonNube); //Se llama al método del servicio pasandole como parámetro el objeto usuario que ya contiene la info
	}
}
