package com.springboot.web.pokemon.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.web.pokemon.models.PokemonModel;
import com.springboot.web.pokemon.service.PokemonService;

@RestController
@RequestMapping("/pokemon")
public class PokemonController {
	
	//Importar el servicio con Autowired para que se instancie automáticamente por spring
		@Autowired
		PokemonService pokemonService;
	
	@PostMapping("/crear") //Insertar información de un Pokemon.
	public PokemonModel crearPokemon(@RequestBody PokemonModel pokemon) { //Recibe como parámetro un @RequestBody, se usa para tomar la información y la guarda en el objeto pokemon
		return pokemonService.guardarPokemon(pokemon); //Se llama al método del servicio pasandole como parámetro el objeto usuario que ya contiene la info
	}
	
	@PutMapping("/actualizar/{id}")
	public ResponseEntity<?> editar(@RequestBody PokemonModel pokemon, @PathVariable Integer id){
		Optional<PokemonModel> opt = pokemonService.obtenerPorId(id);
		if (!opt.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		// Para no modificar todo el objeto, se especifican solo los que sí.
		PokemonModel pokemonDb = opt.get();
		pokemonDb.setNombre(pokemon.getNombre());
		pokemonDb.setGenero(pokemon.getGenero());
		pokemonDb.setHabilidad(pokemon.getHabilidad());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(pokemonService.guardarPokemon(pokemonDb));
	}
	
	@GetMapping(path = "/consultar/{id}")
	public ResponseEntity<?> listar(){
		return ResponseEntity.ok().body(pokemonService.obtenerPokemones());
	}
	
	@DeleteMapping(path = "/eliminar/{id}")
	public String eliminarPorId(@PathVariable("id")Integer id) {
		boolean ok = pokemonService.eliminarPokemon(id);
		if (ok) {
			return "Se elimino el pokemon con el id " + id;
		}else {
			return "Error al intentar eliminar el pokemon con el id " + id;
		}
	}

}
