# POKEMON-BACKEND
Desarrollo de proyecto con Spring Boot para gestión de Pokemon.

## Spring Boot
Se recomienda el IDE de Visual Studio Code o IntelliJ, tener instalada la versión 1.8 de Java.

## Ambientes
En la carpeta de resources se debe contar con el archivo application.properties.